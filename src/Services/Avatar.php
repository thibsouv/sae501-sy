<?php
namespace App\Services;
use Multiavatar;

class Avatar{
    public function getAvatar(){
        $multiavatar = new Multiavatar();
        $nbavatar = random_int(0, 9999999999);
        $nbavatar = strval($nbavatar);
        $svgCode = $multiavatar($nbavatar, null, null); 
        return $svgCode;
    }
}
