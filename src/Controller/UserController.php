<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Entity\Commune;
use Doctrine\ORM\EntityManagerInterface;

use App\Services\Avatar;


class UserController extends AbstractController
{
    #[Route('/', name: 'app_user')]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $users = $entityManager->getRepository(User::class)->findAll();

        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            'users' => $users,
        ]);
    }

    #[Route('/new_avatar', name: 'new_avatar_user')]
    public function new_avatar(EntityManagerInterface $entityManager): Response
    {
        $avatar = new Avatar();
        // change size
        $IMG_Avatar = $avatar->getAvatar();

        return $this->render('services/index.html.twig', [
            'avatar'=>$IMG_Avatar,
        ]);
    }

    #[Route('/communes', name: 'communes')]
    public function list_commune(EntityManagerInterface $entityManager): Response
    {
        $communes = $entityManager->getRepository(Commune::class)->findAll();

        return $this->render('communes/list_commune.html.twig', [
            'controller_name' => 'UserController',
            'communes' => $communes,
        ]);
    }
}
