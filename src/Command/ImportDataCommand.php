<?php

namespace App\Command;

use App\Entity\Commune;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:import:data',
    description: 'Add a short description for your command',
)]
class ImportDataCommand extends Command
{
    private $dataAll = [];

    public EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    public function importCVS($arg1)
    {
        $row = 1;
        if (($handle = fopen($arg1, "r")) !== FALSE) {
            $dataC = [];
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                array_push($dataC, $data);
            }
            echo ("Création du tableau terminer \n");
            fclose($handle);
        }
        $this->dataAll = $dataC;
    }

    public function CreaCommune()
    {
        foreach ($this->dataAll as $value) {
            $commune = new Commune();
    
            $commune->setCodeInsee($value[0]);
            $commune->setNameCommune($value[1]);
            $commune->setCodePostal($value[2]);
            $commune->setLibelle($value[3]);
    
            $this->entityManager->persist($commune);
            $this->entityManager->flush();
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        $this->importCVS($arg1);
        $this->CreaCommune();

        $io->success('Command Executé avec succès !');

        return Command::SUCCESS;
    }
}
