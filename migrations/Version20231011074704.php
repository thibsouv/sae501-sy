<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231011074704 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__commune AS SELECT code_insee, name_commune, code_postal, libelle FROM commune');
        $this->addSql('DROP TABLE commune');
        $this->addSql('CREATE TABLE commune (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, code_insee INTEGER NOT NULL, name_commune VARCHAR(255) NOT NULL, code_postal INTEGER NOT NULL, libelle VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO commune (code_insee, name_commune, code_postal, libelle) SELECT code_insee, name_commune, code_postal, libelle FROM __temp__commune');
        $this->addSql('DROP TABLE __temp__commune');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__commune AS SELECT code_insee, name_commune, code_postal, libelle FROM commune');
        $this->addSql('DROP TABLE commune');
        $this->addSql('CREATE TABLE commune (code_insee INTEGER DEFAULT NULL, name_commune CLOB DEFAULT NULL, code_postal INTEGER DEFAULT NULL, libelle CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO commune (code_insee, name_commune, code_postal, libelle) SELECT code_insee, name_commune, code_postal, libelle FROM __temp__commune');
        $this->addSql('DROP TABLE __temp__commune');
    }
}
